/**
 *@Description This module is to be used to send out emails
 *@module Email
 */
// const nodemailer = require('nodemailer'),
//     environment = process.env.NODE_ENV && process.env.NODE_ENV!== undefined? process.env.NODE_ENV :  'local';
var fs = require('fs');
var extract = require('extract-zip');
var parser = require('xml2json');
var curl = require( 'curl-request' );
var httpBuildQuery = require('http-build-query');
var environment = process.env.NODE_ENV && process.env.NODE_ENV!== undefined ? process.env.NODE_ENV :  'local';

var config = {
    CIPHER_KEY: 'Those who profess to favour freedom and yet depreciate agitation are men who want rain without thunder and lightning',
    port: '443',
    action: '1',
    currency: '414',
    language: 'ENG',
    webaddress: '',
    id: '',
    password: '',
    passwordhash: '',
    transId: '',
    amt: '',
    responseURL: '',
    errorURL: '',
    trackid: '',
    paymentUrl: '',
    paymentId: '',
    context: '',
    resourcePath: '',
    alias: '',
};

exports.getPaymentUrl = function (params, callback) {

    if (!params['amt']){
        return callback(new Error('Amount field empty'));
    }
    if (!params['responseURL']){
        return callback(new Error('Please specify Response Url'));
    }
    if (!params['errorURL']){
        return callback(new Error('Please specify Error Url'));
    }
    if (!params['alias']){
        return callback(new Error('Please specify alias'));
    }
    if (!params['resourcePath']){
        return callback(new Error('Please specify resourcePath'));
    }
    for (key in params)
    {
        if (key in config){
            config[key] = params[key];
        }
    }

    var init = initResourceFile(function (err) {
        if (err)
        {
            return environment == 'local' ? callback(err) : callback(new Error('Error processing transcation!'));
        }
        requestPayment(function (err, paymentUrl) {
            return err ? callback(err) : callback(null,paymentUrl);
        });
    });
};



function requestPayment(callback) {
    processRequest(function (err, payload) {
        if (err){
            return callback(err);
        }
        let paymentURL = payload.indexOf(":");
        config.paymentId = payload.substr(0, paymentURL);
        config.paymentUrl = payload.substr(paymentURL + 1);
        return callback(null,getPaymentURL());
    });
}


function processRequest(callback)
{
    var url = buildUrl();
    var urlParams = buildUrlParams();

    if (!url || !urlParams || !config.webaddress) {
        return callback(new Error("Failed to make connection to the Target URL"));
    }

    var request = new curl();
    request
        .setHeaders([])
        .setBody(urlParams)
        .post(url)
        .then(({statusCode, body, headers}) => {
            return callback(null,body);
        })
        .catch((e) => {
            return callback(e);
        });
}


function buildUrl() {
    let protocol = config.port == "443" ? "https://" : "http://";
    let stringBuffer = protocol + config.webaddress + ':' + config.port;
    stringBuffer += !config.context ? "/" : "/" + config.context + "/";
    stringBuffer += "servlet/PaymentInitHTTPServlet";
    return stringBuffer;
}

function buildUrlParams() {
    config.trackid = 12;
    let params = {
        id: config.id,
        password: config.password,
        passwordhash: config.passwordhash,
        currencycode: config.currency,
        action: config.action,
        langid: config.language,
        responseURL: config.responseURL,
        errorURL: config.errorURL,
        trackid: config.trackId,
        amt: config.amt,
    };
    var query = httpBuildQuery(params);
    return query;
}

function initResourceFile(callback)
{
    parseResourceFile(function (err, payload) {
        if (err)
        {
            return callback(err);
        }
        payload = payload['terminal'];
        if (!payload)
        {
            return false;
        }
        for (key in payload) {
            config[key] = payload[key];
        }
        if (fs.existsSync(config.resourcePath + "resource.cgz")){
            fs.unlink(config.resourcePath + "resource.cgz", function(err){
                console.log('file deleted successfully');
            });
        }
        return callback(null);
    });
}


function parseResourceFile(callback)
{
    var filenameInput = config.resourcePath + "resource.cgn";

    if(!fs.existsSync(filenameInput)) {
        return callback(new Error('Resource file not found on the path ' + config.resourcePath));
    }
    var stats = fs.statSync(filenameInput);
    var contentsInput = fs.readFileSync(filenameInput,'binary').toString();
    var filenameOutput = config.resourcePath + "resource.cgz";
    var inByteArray = getBytes(contentsInput);
    var outByteArray = simpleXOR(inByteArray);

    var finalString = getString(outByteArray);


    var buffer = Buffer.from(finalString,'binary');
    if(fs.existsSync(filenameOutput)) {
        fs.unlinkSync(filenameOutput);
    }
    //

    var write = fs.writeFileSync(filenameOutput,buffer);
    const resolve = require('path').resolve;

    var resolvedUnpackPath = resolve(config.resourcePath);
    var resolvedFilePath = resolve(filenameOutput);

    console.log('FIle ' + resolvedFilePath + fs.existsSync(resolvedFilePath))
    if (!fs.existsSync(resolvedFilePath))
    {
        return callback(new Error('Couldn\'t create file ') + filenameOutput);
    }


    extractZIP(resolvedFilePath, resolvedUnpackPath, function (err) {
        if (err)
        {
            return callback(err);
        }
        var xmlNameInput = config.resourcePath + config.alias + ".xml";

        if (!fs.existsSync(xmlNameInput)){
            return callback(new Error('File '+ xmlNameInput + ' not found!'));
        }

        var xmlContentsInput = fs.readFileSync(xmlNameInput).toString();

        var parsedZip = parseZip(getString(simpleXOR(getBytes(xmlContentsInput))));
        if (!parsedZip instanceof Array) {
            console.log('Parsed Zip file returned Wrong Format, Should be an array');
        }
        return callback(null,parsedZip);
    });


}

function extractZIP(resolvedFilePath, resolvedUnpackPath, callback) {
    var sync = require('sync');
    sync(function () {
        extract(resolvedFilePath, {dir: resolvedUnpackPath}, function (err) {
            // extraction is complete. make sure to handle the err
            if (err){
                console.log('Could not open the Zip file ' + err); //Check this
                return callback(new Error('Could not open the Zip file ' + err));
            }
            return callback(null);
        });
    })
}

function simpleXOR(abyte0) {
    let key = config.CIPHER_KEY;
    let abyte1 = getBytes(key);

    let abyte2 = [];
    for (let i = 0; i < abyte0.length;) {
        for (let j = 0; j < abyte1.length; j++) {
            abyte2[i] = xor(abyte0[i] , abyte1[j]);
            if (++i == abyte0.length)
                break;
        }
    }
    return abyte2;
}

function getBytes(string) {
    // console.log(string)
    let hexArray = [];
    let size = string.length;
    for (let i = 0; i < size; i++) {
        hexArray[i] = String.fromCharCode(string[i].charCodeAt(0));
    }
    return hexArray;
}


function parseZip(zip)
{
    return JSON.parse(parser.toJson(zip));
}

function getString(byteArray)
{
    var finalString = '';
    for(var i = 0; i < byteArray.length; i++)
        finalString += byteArray[i];
    return finalString;
}

function xor(a, b) {
    var res = '';
    if (a.length > b.length) {
        for (var i = 0; i < b.length; i++) {
            res += String.fromCharCode(a[i].charCodeAt(0).toString(10) ^ b[i].charCodeAt(0).toString(10));
        }
    } else {
        for (var i = 0; i < a.length; i++) {
            res += String.fromCharCode(a[i].charCodeAt(0).toString(10) ^ b[i].charCodeAt(0).toString(10));
        }
    }
    return res;
}

function getPaymentURL()
{
    return config.paymentUrl + "&PaymentID=" + config.paymentId;
}