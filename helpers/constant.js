module.exports = {
    VERSION : "v1",
    PAGINATION : {
        DEFAULT_PAGE : 1,
        DEFAULT_ROW_COUNT : 10
    }
};