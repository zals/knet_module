# KNET Payment Gateway

# Installation
npm i knet-payment

# For previous node versions
> node v6.x.x and v8.x.x use fibers v2.0.2
```JSON
"devDependencies": {
   "fibers": "~2.0.2"
}
```

# Usage
```JavaScript
var knet_helper = require('knet-payment');
function getPaymentUrl(params , callback) {
    params.assign(
      {resourcePath : 'Data/KNET/'},
        {responseURL: '/response'},
        {errorURL: '/error'},
        {alias: 'test_alias'}
    );
    knet_helper.getPaymentUrl(params, function(err, paymentUrl){
        if (err){
            return callback(err);
        }
        return callback(null, paymentUrl)
    });
}
```


# Parameters required

> resourcePath - filepath to resource.cgn
> responseURL - route to handle resource,
> errorURL - route to handle error,
> alias - alias specified for resource file
> amt - Amount
